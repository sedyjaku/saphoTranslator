package com.translate;

import com.model.Punctuation;
import com.model.Word;
import com.model.WordType;

import java.util.List;
import java.util.ListIterator;

/**
 * Created by EnXer on 24.06.2018.
 */
public class PigLatinTranslator implements Translator{

    private static final String VOWEL_SUFFIX="way";
    private static final String CONSONANT_SUFFIX="ay";


    private String splitText;

    /**
     * Splits text into words with separator space and translates it into pig-latin
     * @param text text to be translated
     * @return translated text
     */
    @Override
    public String translate(String text) {
        String finalText="";
        String [] words= text.split(" ");
        for(String word : words){
                finalText+=translateWord(word)+" ";
        }
        return finalText.substring(0,finalText.length()-1);
    }

    /**
     * Checks whether word has hyphen or not
     * @param word word to be checked
     * @return true if has hyphen, false otherwise
     */
    private boolean hasHyphen(String word) {
        if(word.contains("-")){
            return true;
        }
        return false;
    }

    /**
     * translates word into pig-latin. If it has hyphen, its called recursively on splitted words by hyphen
     * @param word word to translate
     * @return translated word
     */
    private String translateWord(String word){
        String finalWord;
        if(hasHyphen(word)){
            String[] words=word.split("-");
            finalWord=new String();
            for(String hyphenWord : words){
                finalWord+=translateWord(hyphenWord)+"-";
            }
            finalWord=finalWord.substring(0,finalWord.length()-1);
            return finalWord;
        }
        Word translateFrom=new Word(word);
        switch(translateFrom.getWordType()){
            case WAY_ENDER:
                finalWord=translateWayEnder(translateFrom);
                break;
            case CONSONANT_FIRST:
                finalWord=translateConsonant(translateFrom);
                break;
            case VOWEL_FIRST:
                finalWord=translateVowel(translateFrom);
                break;
            default:
                return word;
        }
        return finalWord;
    }

    /**
     * translates word with vowel rules
     * @param translateFrom word to be translated by vowel rules
     * @return translated word
     */
    private String translateVowel(Word translateFrom) {
        String letterText=translateFrom.getLetterText();
        String finalWord=letterText;
        finalWord=finalWord.toLowerCase()+VOWEL_SUFFIX;
        finalWord=addPunctuations(finalWord,translateFrom.getPunctuationList());
        finalWord=addCapitalLetters(finalWord,translateFrom.getCapitalLettersPositionList());
        return finalWord;
    }


    /**
     * translates word with consonant rules
     * @param translateFrom word to be translated by consonant rules
     * @return translated word
     */
    private String translateConsonant(Word translateFrom) {
        String letterText=translateFrom.getLetterText();
        String finalWord=letterText.substring(1,letterText.length())+letterText.charAt(0);
        finalWord=finalWord.toLowerCase()+CONSONANT_SUFFIX;
        finalWord=addPunctuations(finalWord,translateFrom.getPunctuationList());
        finalWord=addCapitalLetters(finalWord,translateFrom.getCapitalLettersPositionList());
        return finalWord;
    }

    /**
     * adds capital letters to the word based on pig-latin rules
     * @param finalWord word to be edited
     * @param capitalLettersPositionList position of capital letters
     * @return word with proper capital letters based on pig-latin rules
     */
    private String addCapitalLetters(String finalWord, List<Integer> capitalLettersPositionList) {
        for(Integer i : capitalLettersPositionList){
            finalWord=finalWord.substring(0,i)+Character.toUpperCase(finalWord.charAt(i))+finalWord.substring(i+1);
        }
        return finalWord;
    }

    /**
     * adds punctuation to the word based on pig-latin rules
     * @param finalWord word to be edited
     * @param punctuationList list of punctuations to be added
     * @return word with proper punctuation based on pig-latin rules
     */
    private String addPunctuations(String finalWord, List<Punctuation> punctuationList) {
        ListIterator<Punctuation> it = punctuationList.listIterator(punctuationList.size());
        while (it.hasPrevious()){
            Punctuation punctuation=it.previous();
            finalWord=finalWord.substring(0,finalWord.length()-punctuation.getPosition()+1)+punctuation.getPunctuationCharacter()+finalWord.substring(finalWord.length()-punctuation.getPosition()+1,finalWord.length());
        }
        return finalWord;
    }

    /**
     * translates word with "way" ending
     * @param translateFrom word to be translated from
     * @return translated word
     */
    private String translateWayEnder(Word translateFrom) {
        return translateFrom.getText();
    }


}
