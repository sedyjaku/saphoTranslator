package com.translate;

/**
 * Created by EnXer on 24.06.2018.
 */
public interface Translator {

    public String translate(String text);
}
