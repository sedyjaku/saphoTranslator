package com.factory;

import com.translate.PigLatinTranslator;
import com.translate.Translator;

/**
 * Created by EnXer on 24.06.2018.
 */
public class PigLatinTranslatorFactory implements TranslatorFactory{
    @Override
    public Translator createTranslator() {
        return new PigLatinTranslator();
    }
}
