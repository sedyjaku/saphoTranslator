package com.factory;

import com.translate.Translator;

/**
 * Created by EnXer on 24.06.2018.
 */
public interface TranslatorFactory {
    public Translator createTranslator();
}
