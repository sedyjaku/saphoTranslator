package com.model;

import java.util.LinkedList;
import java.util.List;

import static java.lang.Character.isUpperCase;

/**
 * Created by EnXer on 24.06.2018.
 */
public class Word {


    private static final String WAY_ENDER_TEXT="way";
    WordType wordType;
    List<Integer> capitalLettersPositionList;
    String text;
    String letterText;
    List<Punctuation> punctuationList;

    public Word(String text) {
        letterText=new String();
        this.text = text;
        computeWordType();
        capitalLettersPositionList=new LinkedList<>();
        punctuationList=new LinkedList<>();
        analyzeWord();
    }

    /**
     * Finds all upper case letters and add their position to capitalLettersPositionList, also finds all punctuations and adds them to punctuationList and creates word without punctuation in letterText field
     */
    private void analyzeWord() {
        for(int i=0;i<text.length();i++){
            if(isUpperCase(text.charAt(i))){
                capitalLettersPositionList.add(i);
            }
            if(Punctuation.isPunctuation(text.charAt(i))){
                Punctuation punctuation=new Punctuation(text.charAt(i),text.length()-i);
                punctuationList.add(punctuation);
                continue;
            }

            letterText+=text.charAt(i);
        }
    }

    /**
     * computes type of word based on WordType
     */
    private void computeWordType() {
        if(text.endsWith(WAY_ENDER_TEXT)){
            wordType=WordType.WAY_ENDER;
            return;
        }
        if(startsWithVowel(text)){
            wordType=WordType.VOWEL_FIRST;
        }
        else{
            wordType=WordType.CONSONANT_FIRST;
        }
        return;
    }

    /**
     * checks if word starts with vowel
     * @param text to check if starts with vowel
     * @return true if text starts with vowel, false otherwise
     */
    private boolean startsWithVowel(String text){
        String vowels="aeiou";
        if (vowels.indexOf(Character.toLowerCase(text.charAt(0))) != -1) {
            return true;
        }
        return false;
    }


    public WordType getWordType() {
        return wordType;
    }

    public void setWordType(WordType wordType) {
        this.wordType = wordType;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }



    public String getLetterText() {
        return letterText;
    }

    public List<Integer> getCapitalLettersPositionList() {
        return capitalLettersPositionList;
    }

    public void setCapitalLettersPositionList(List<Integer> capitalLettersPositionList) {
        this.capitalLettersPositionList = capitalLettersPositionList;
    }

    public void setLetterText(String letterText) {
        this.letterText = letterText;
    }

    public List<Punctuation> getPunctuationList() {
        return punctuationList;
    }

    public void setPunctuationList(List<Punctuation> punctuationList) {
        this.punctuationList = punctuationList;
    }
}
