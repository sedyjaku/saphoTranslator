package com.model;

import static java.lang.Character.isLowerCase;
import static java.lang.Character.isUpperCase;

/**
 * Created by EnXer on 24.06.2018.
 */
public class Punctuation  {
    char punctuationCharacter;
    int position;

    public Punctuation(char punctuationCharacter, int position) {

        this.punctuationCharacter = punctuationCharacter;
        this.position = position;
    }
    public char getPunctuationCharacter() {
        return punctuationCharacter;
    }

    public void setPunctuationCharacter(char punctuationCharacter) {
        this.punctuationCharacter = punctuationCharacter;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    /**
     * checks whether character is a letter or not
     * @param c character to check
     * @return true if character is not a letter (a-z/A-Z)
     */
    public static boolean isPunctuation(char c){
        if(isUpperCase(c))
            return false;
        if(isLowerCase(c))
            return false;
        return true;
    }
}
