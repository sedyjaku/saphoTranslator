package com.tests;

import com.factory.PigLatinTranslatorFactory;
import com.factory.TranslatorFactory;
import com.translate.Translator;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by EnXer on 24.06.2018.
 */
class PigLatinTranslatorTest {

    @org.junit.jupiter.api.Test
    void testConsolant() {
        TranslatorFactory factory = new PigLatinTranslatorFactory();
        Translator translator=factory.createTranslator();
        String toTest=translator.translate("Hello ");
        String expected="Ellohay";
        assertEquals(expected, toTest);
    }
    @org.junit.jupiter.api.Test
    void testVowel() {
        TranslatorFactory factory = new PigLatinTranslatorFactory();
        Translator translator=factory.createTranslator();
        String toTest=translator.translate("apple");
        String expected="appleway";
        assertEquals(expected, toTest);
    }
    @org.junit.jupiter.api.Test
    void testWayEnding() {
        TranslatorFactory factory = new PigLatinTranslatorFactory();
        Translator translator=factory.createTranslator();
        String toTest=translator.translate("stairway");
        String expected="stairway";
        assertEquals(expected, toTest);
    }
    @org.junit.jupiter.api.Test
    void testPunctuation() {
        TranslatorFactory factory = new PigLatinTranslatorFactory();
        Translator translator=factory.createTranslator();
        String toTest=translator.translate("can’t");
        String expected="antca’y";
        assertEquals(expected, toTest);
    }
    @org.junit.jupiter.api.Test
    void testPunctuation2() {
        TranslatorFactory factory = new PigLatinTranslatorFactory();
        Translator translator=factory.createTranslator();
        String toTest=translator.translate("end.");
        String expected="endway.";
        assertEquals(expected, toTest);
    }

    @org.junit.jupiter.api.Test
    void testHyphens() {
        TranslatorFactory factory = new PigLatinTranslatorFactory();
        Translator translator=factory.createTranslator();
        String toTest=translator.translate("this-thing");
        String expected="histay-hingtay";
        assertEquals(expected, toTest);
    }

    @org.junit.jupiter.api.Test
    void testCapital() {
        TranslatorFactory factory = new PigLatinTranslatorFactory();
        Translator translator=factory.createTranslator();
        String toTest=translator.translate("McCloud");
        String expected="CcLoudmay";
        assertEquals(expected, toTest);
    }
    @org.junit.jupiter.api.Test
    void testCapital2() {
        TranslatorFactory factory = new PigLatinTranslatorFactory();
        Translator translator=factory.createTranslator();
        String toTest=translator.translate("Beach");
        String expected="Eachbay";
        assertEquals(expected, toTest);
    }

}